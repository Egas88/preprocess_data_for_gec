import random
from pymorphy2 import MorphAnalyzer
import pandas as pd

morph = MorphAnalyzer()


def change_rand_char(text):
    text = str(text)
    text = text.split()
    if len(text) == 1 or len(text) == 0:
        return ' '.join(text)
    word_ind = random.randint(0, len(text) - 1)
    word = text[word_ind]
    index = random.randint(0, len(word) - 1)
    new_letter = chr(random.randint(1072, 1103))
    word = word[:index] + new_letter + word[index + 1:]
    text[word_ind] = word
    return ' '.join(text)


def add_random_char(text):
    text = str(text)
    text = text.split()
    if len(text) == 1 or len(text) == 0:
        return ' '.join(text)
    word_ind = random.randint(0, len(text) - 1)
    word = text[word_ind]
    index = random.randint(0, len(word) - 1)
    new_letter = chr(random.randint(1072, 1103))
    word = word[:index] + new_letter + word[index:]
    text[word_ind] = word
    return ' '.join(text)


def swap_rand_chars(text):
    text = str(text)
    text = text.split()
    if len(text) == 1 or len(text) == 0:
        return ' '.join(text)
    word_ind = random.randint(0, len(text) - 1)
    word = text[word_ind]
    if len(word) == 1:
        return ' '.join(text)
    type = random.randint(0, 1)
    if type == 0:
        index = random.randint(0, len(word) - 2)
        l = list(word)
        l[index], l[index+1] = l[index+1], l[index]
        word = ''.join(l)
        text[word_ind] = word
    else:
        index = random.randint(1, len(word) - 1)
        l = list(word)
        l[index - 1], l[index] = l[index], l[index - 1]
        word = ''.join(l)
        text[word_ind] = word
    return ' '.join(text)


def split_rand_word(text):
    text = str(text)
    text = text.split()
    if len(text) == 1 or len(text) == 0:
        return ' '.join(text)
    word_ind = random.randint(0, len(text) - 1)
    word = text[word_ind]
    index = random.randint(0, len(word) - 1)
    w1 = word[:index]
    w2 = word[index:]
    text[word_ind] = w1
    text.insert(word_ind+1, w2)
    return ' '.join(text)


def normalize_rand_word(text):
    text = str(text)
    text = text.split()
    if len(text) == 1 or len(text) == 0:
        return ' '.join(text)
    word_ind = random.randint(0, len(text) - 1)
    word = text[word_ind]
    word = morph.normal_forms(word)[0]
    text[word_ind] = word
    return ' '.join(text)


def change_word_case(text):
    text = str(text)
    text = text.split()
    if len(text) == 1 or len(text) == 0:
        return ' '.join(text)
    word_ind = random.randint(0, len(text) - 1)
    word = text[word_ind]
    cases = ['nomn', 'gent', 'datv', 'accs', 'ablt', 'loct']
    _case = random.choice(cases)
    changed_word = morph.parse(word)[0].inflect({'sing', _case})
    if changed_word is None:
      return ' '.join(text)
    text[word_ind] = changed_word.word
    return ' '.join(text)


def change_word_number(text):
    text = str(text)
    text = text.split()
    if len(text) == 1 or len(text) == 0:
        return ' '.join(text)
    word_ind = random.randint(0, len(text) - 1)
    word = text[word_ind]
    changed_word = morph.parse(word)[0].make_agree_with_number(random.randint(1, 5))
    if changed_word is None:
        return ' '.join(text)
    text[word_ind] = changed_word.word
    return ' '.join(text)


def delete_rand_char(text):
    text = str(text)
    text = text.split()
    if len(text) == 1 or len(text) == 0:
        return ' '.join(text)
    word_ind = random.randint(0, len(text) - 1)
    word = text[word_ind]
    if len(word) < 2:
        return ' '.join(text)
    index = random.randint(0, len(word) - 2)
    word = word[:index] + word[index + 1:]
    text[word_ind] = word
    return ' '.join(text)


def swap_random_words(text):
    text = str(text)
    text = text.split()
    if len(text) == 1 or len(text) == 0:
        return ' '.join(text)
    type = random.randint(0, 1)
    if type == 0:
        index = random.randint(0, len(text) - 2)
        text[index], text[index + 1] = text[index + 1], text[index]
    else:
        index = random.randint(1, len(text) - 1)
        text[index - 1], text[index] = text[index], text[index - 1]
    return ' '.join(text)


def lowercase(text):
    text = str(text)
    return text.lower()


def remove_random_word(text):
    text = str(text)
    text = text.split()
    if len(text) == 1 or len(text) == 0:
        return ' '.join(text)
    index = random.randint(0, len(text) - 1)
    text.pop(index)
    return ' '.join(text)


def add_comma_symbols(text):
    text = str(text)
    text = text.split()
    if len(text) == 1 or len(text) == 0 or len(text) == 2:
        return ' '.join(text)
    index = random.randint(1, len(text) - 2)
    commas = [',', ':', ';', '-']
    new_comma = commas[random.randint(0, 3)]
    text[index] = text[index] + new_comma
    return ' '.join(text)



df = pd.read_csv('lenta-ru-news.csv')
print(df.head())
df.to_csv('main_data.csv')
df.rename(columns={'text': 'output'}, inplace=True)
df = df.drop(columns=['url', 'title', 'topic', 'tags', 'date'], axis=1)
new_df = df
new_df['output'] = df['output'].str.split('.')
new_df = new_df.explode('output').astype(str)
new_df = new_df.reset_index(drop=True)
new_df = new_df[:1000000]
print(len(new_df.index))
new_df['input'] = ''
new_df = new_df[['input', 'output']]
functions = [change_rand_char, add_random_char, swap_rand_chars, split_rand_word, normalize_rand_word, change_word_number, delete_rand_char, swap_random_words, lowercase, remove_random_word, add_comma_symbols]
new_df['input'] = new_df['output'].apply(random.choice(functions)).apply(random.choice(functions)).apply(random.choice(functions))
new_df.to_csv('main_data.csv')
